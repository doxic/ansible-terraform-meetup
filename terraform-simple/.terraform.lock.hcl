# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "2.17.0"
  constraints = "~> 2.17.0"
  hashes = [
    "h1:Y2YvQIMTWnl2xz82+5gOVo3zSgQFpk7o/CadXP+Ad6w=",
    "zh:05d97d21dea9acc20cb88885e658508bf23a09d2561eb203be70248f9bb7ee68",
    "zh:23ee7136382586ca1f6e5a2df6751a22e29cd4060dab0bc613b702b2edf3881c",
    "zh:2fc9647ded9459118966b6883372c4707f232220525ef56b488de738cf9c5e78",
    "zh:31624f7efb0c93461cb54f5197c6af891ba8ccb540362356b5722509a00f3976",
    "zh:3ded9720928dac2b6027e3c074511f9717726fd2e67feb17f764a3766ff22d3f",
    "zh:623c689e580dd3cd4d75a713e40583590d343ceb801e6ffbc6d90d43be85cf6f",
    "zh:63191d6bf16c9307d98ee4c31637e74687599baa05266218c5a32ed22f28b06b",
    "zh:697955f294fff567b3862e6d0e7f2bbcfcfa8bfb58be28efca2fda00d9b7ebaa",
    "zh:d4980aacac7f2e6c24e9072b284af77f0c5ad485c48a1ff041efeee3beacec58",
    "zh:e5d4e2595034fc6a7382409aef91425d8a21473532774edce60166205ee0e4af",
    "zh:f1b2f956748aba79be2f74b68035055cc99fdb6cdb2b5183e119be00c13bcda4",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "5.16.1"
  constraints = "5.16.1"
  hashes = [
    "h1:4xM59GFsk7M7VoFFMRsX6lSKuxCNFJRm5jUI9ZUqztM=",
    "zh:0d313783ae3abe06387b28b8c9f9b363f7b9cc3cecb7af879220f218a34d6463",
    "zh:35bdb7a69f418dc71e3aa7217bacc4849dea33889d338d76f0a2bcc691073843",
    "zh:38ec5e1220caccec76855084971a88a08cb2dc3ed33ace5399d8a1785982b935",
    "zh:4b635ae8e21edc9deb9a83c4e1c8d1bfc9482ad930d18ba22047402d54d7edad",
    "zh:4cf88464ab0fdbd3e21795593930a81c21ce573c279fa58be23d40005625196c",
    "zh:5b52f24bd51b31b466f83243a6e3f1744bfad3e87989db47c2a6cc4c7b9d5141",
    "zh:5ffc79bd15889b9232b226a29b3431659e1a30c476178dc9b59af5b7f35ac5fb",
    "zh:64630c725983161ad99f97f1aabc0106a1f4710805fe4b5b27b8642ddad56cf5",
    "zh:8f27b067e565c4a884f99cfa1eadeae60cc7535cdf2189de529df41b9c2b95c6",
    "zh:97e72c4ed08c3176446464ebffde3b1304848fb9c762b14bce16a5e9d7ae3c96",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:a53a76386100ad69a406ca80e04152fcb0a63ae12b55b0c876ae007e11627a9e",
    "zh:c9b864cd00d79c5ce8807673f4a6e386f44ff49bbdb56ee51086b403dc064347",
    "zh:e139f7d6ed11fa72838b77898e6dff70f3d001ff37369a83f575e658b1912054",
    "zh:e68cd728ed67bf4dc5e7c0e3e3082b732c768fd459a23ae6f691ac734d59b9c7",
  ]
}
