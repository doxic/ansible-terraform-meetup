terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "4.14.0"
    }
    aws = {
      source = "hashicorp/aws"
      version = "5.16.1"
    }
  }
}

provider "cloudflare" {}
provider "aws" {}

resource "aws_instance" "instance1" {
  ami           = "ami-053b0d53c279acc90"
  instance_type = "t2.micro"

  tags = {
    Name = "instance1"
  }
}

resource "cloudflare_record" "instance1" {
  zone_id = data.cloudflare_zone.bbwin.id
  type    = "A"
  name    = "instance1"
  value   = aws_instance.instance1.public_ip
}

data "cloudflare_zone" "bbwin" {
  name = "bbwin.ch"
}