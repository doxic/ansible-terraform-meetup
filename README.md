# Ansible Meetup: Integrating Ansible & Terraform

Sep 12, 2023  
Hönggerstrasse 65, Zürich, ZH

## Prerequisites

- This repo uses Taskfile, use the official documentation for [installation instructions](https://taskfile.dev/installation/). All commands used are in `Taskfile.yml`
- For Terraform, use the official documentation for your distribution. [Install | Terraform | HashiCorp Developer](https://developer.hashicorp.com/terraform/downloads?product_intent=terraform)
- Ansible installed with `pip`, command provided for Ubuntu. If you chose not to Taskfile, the instructions are in the headers of `requirements.txt` and `requirements.yml`.

## Slides
Slides created with Obsidian and Advanced Slides (reveal.js).

## Demo
- [Dominic Rüttimann / AWS Enablement Day · GitLab](https://gitlab.com/doxic/aws-enablement-day)