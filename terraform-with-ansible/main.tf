terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "4.14.0"
    }
    aws = {
      source = "hashicorp/aws"
      version = "5.16.1"
    }
    ansible = {
      source = "ansible/ansible"
      version = "1.1.0"
    }
  }
}

provider "ansible" {}
provider "cloudflare" {}
provider "aws" {}

data "cloudflare_zone" "bbwin" {
  name = "bbwin.ch"
}

resource "aws_instance" "instance1" {
  ami           = "ami-053b0d53c279acc90"
  instance_type = "t2.micro"
  key_name      = "vockey"
  vpc_security_group_ids = [aws_security_group.ssh.id]

  tags = {
    Name = "instance1"
  }
}

resource "aws_security_group" "ssh" {
  name        = "ssh"
  description = "Allow SSH inbound traffic"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "cloudflare_record" "instance1" {
  zone_id = data.cloudflare_zone.bbwin.id
  type    = "A"
  name    = "instance1"
  value   = aws_instance.instance1.public_ip
}


resource "ansible_host" "instance1" {
	name   = cloudflare_record.instance1.hostname

	variables = {
		ansible_host = aws_instance.instance1.public_ip
	}
}

resource "ansible_group" "workshop" {
  name = "workshop"
  variables = {
    ansible_user                 = "ubuntu"
    ansible_ssh_private_key_file = "../vockey"
  }
}

resource "ansible_playbook" "bootstrap" {
  playbook   = "bootstrap.yml"

  name = ansible_host.instance1.name
  groups = ["workshop"]

  replayable = true

  extra_vars = {
    ansible_user = "ubuntu",
    ansible_ssh_private_key_file = "../vockey"
  }

  verbosity = 1
  ignore_playbook_failure = true
}

# output stderr
output "playbook_stderr" {
  value = ansible_playbook.bootstrap.ansible_playbook_stderr
}

# output stdout
output "playbook_stdout" {
    value = ansible_playbook.bootstrap.ansible_playbook_stdout
}
